//
//  ViewController.swift
//  FBFeed
//
//  Created by emel.elias on 08/02/17.
//  Copyright © 2017 emel.elias. All rights reserved.
//

import UIKit
import FacebookLogin
import FacebookCore
class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let loginButton = LoginButton(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 20), publishPermissions: [])
        loginButton.center = self.view.center
        self.view.addSubview(loginButton)
        
        
        let connection = GraphRequestConnection()
        connection.add(GraphRequest(graphPath: "/6815841748/feed",parameters:["fields":"attachments.limit(10){url,subattachments,media,type,description,target,description_tags,title}"])) { httpResponse, result in
            switch result {
            case .success(let response):
               // print("Graph Request Succeeded: \(response)")
                
                let responseDetails = response as GraphResponse
                
                
                print("Graph Request Succeeded: \(responseDetails.dictionaryValue)")
               
                
            case .failed(let error):
                print("Graph Request Failed: \(error)")
            }
        }
        connection.start()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

